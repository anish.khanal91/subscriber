import React from "react";
const Button = ({ className, text, type, variant }) => (
  <button
    className={`${className} border-none px-7 py-3 outline-none rounded-md text-sm text-pure-white ${
      variant === "primary"
        ? "bg-black"
        : variant === "secondary"
        ? "bg-red-700"
        : variant === "outline"
        ? "outline-2"
        : null
    }`}
    type={type}
  >
    {text}
  </button>
);

export default React.memo(Button);
