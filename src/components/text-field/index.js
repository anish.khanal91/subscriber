import React from "react";
import { ErrorMessage, useField } from "formik";

const TextField = ({
  placeholder,
  type,
  readOnly,
  defaultValue,
  hidden,
  ref,
  ...otherProps
}) => {
  const [field, meta] = useField(otherProps);
  return (
    <div className="mb-1">
      <input
        className={`w-full appearance-none ring-2 ring-[#E5E5E5] rounded-md h-14 block px-3 placeholder:text-[#838383] placeholder:text-base bg-white  focus:outline-none focus:shadow-outline  ${
          meta.touched && meta.error
            ? "ring-[#dc3545] ring-2 outline-none placeholder:text-[#dc3545]"
            : ""
        } ${!meta.error ? "is-valid" : ""}`}
        {...field}
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        autoComplete="off"
        readOnly={readOnly}
      />
      <ErrorMessage
        component="div"
        name={field.name}
        className="bg-[#e4655f] text-pure-white mt-1 text-xs inline-block py-1 px-2 mb-3"
      />
    </div>
  );
};

export default React.memo(TextField);
