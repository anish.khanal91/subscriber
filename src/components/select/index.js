import React from "react";

const Select = ({
  values,
  handleChange,
  handleBlur,
  selectOptionsData,
  placeHolder,
  name,
}) => {
  return (
    <div className="relative">
      <div className="absolute right-3 z-50 inset-y-1/2">
        <svg
          width="12"
          height="7"
          viewBox="0 0 12 7"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 0L6 6.42587L12 1.12354e-06"
            fill="#202020"
          />
        </svg>
      </div>

      <select
        name={name}
        value={values}
        onChange={handleChange}
        onBlur={handleBlur}
        className="w-full text-[#838383] capitalize relative appearance-none ring-2 ring-[#E5E5E5] rounded-md h-14 block px-3 placeholder:text-[#838383] placeholder:text-base bg-white  focus:outline-none focus:shadow-outline"
      >
        <option disabled>{placeHolder}</option>
        {selectOptionsData?.map((items) => (
          <option value={items?.value} key={items?.value}>
            {items?.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default React.memo(Select);
