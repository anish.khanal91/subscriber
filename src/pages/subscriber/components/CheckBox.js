import React from "react";
import { Field } from "formik";

const CheckBox = ({ value, name, text }) => {
  return (
    <div className="relative w-[120px] h-[36px] mr-4 last:mr-0 rounded-md my-2 md:my-0">
      <Field
        type="checkbox"
        name={name}
        value={value}
        className="absolute w-full h-full rounded-md left-0 opacity-0 cursor-pointer"
      />
      <div className="w-full text-xs capitalize text-[#565656] h-full bg-[#F1F1F1] rounded-md flex items-center justify-center">
        {text}
      </div>
    </div>
  );
};

export default React.memo(CheckBox);
