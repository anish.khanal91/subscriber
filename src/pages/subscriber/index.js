import React, { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import CheckBox from "./components/CheckBox";
import { Button, Select, TextField } from "src/components";
import axios from "axios";
import MultiSelect from  'multiselect-react-dropdown'
// Form validation
const FORM_VALIDATION = Yup.object().shape({
  firstName: Yup.string()
    .min(3, "First name must be at least 3 character long")
    .required("First Name is required"),
  lastName: Yup.string()
    .min(3, "Last name must be at least 3 character long")
    .required("Last Name is required"),
  email: Yup.string()
    .email()
    .typeError("Please enter a valid email address")
    .required("First Name is required"),
  interest: Yup.array().min(1).of(Yup.string().required()).required("Required"),
  country: Yup.string().notRequired(),
  phone: Yup.string().notRequired(),
  gender: Yup.string().notRequired(),
  postCode: Yup.string().notRequired(),
});
const Subscriber = () => {
  const [response, setResponse] = useState("");
  const [width, setWidth] = useState(window.innerWidth);
  function handleWindowSizeChange() {
    setWidth(window.innerWidth);
  }
  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  const selectOptionsCountry = [
    { label: "Australia", value: "Australia" },
    { label: "Nepal", value: "Nepal" },
  ];
  const selectOptionsGender = [
    { label: "Female", value: "Female" },
    { label: "Male", value: "Male" },
  ];
  const selectOptionsInterest = [
    { id:1, label: "Fresh Food", value: "Fresh Food" },
    { id:2, label: "entertainment", value: "entertainment" },
    { id:3, label: "services", value: "services" },
    { id:4, label: "beauty & Health", value: "beauty & Health" },
  ];
  const [options] = useState(selectOptionsInterest);
  const fetchPostApiData = async () => {
    const res = await axios.post(
      "https://run.mocky.io/v3/21b54027-09ac-40cf-92d5-e6a0f7af69d0"
    );
    setResponse(res?.data);
  };
  const initialValue = {
    firstName: "",
    lastName: "",
    email: "",
    interest: [],
    phone: "",
    country: "",
    gender: "",
    postCode: "",
  };

  const FormData = [
    { id: 0, placeholder: "First Name", name: "firstName", type: "text" },
    { id: 1, placeholder: "Last Name", name: "lastName", type: "text" },
    { id: 2, placeholder: "Email", name: "email", type: "email" },
    { id: 3, placeholder: "Phone", name: "phone", type: "text" },
    { id: 4, placeholder: "Postcode", name: "postCode", type: "text" },
  ];

  return (
    <div className="container mx-auto bg-pure-white rounded-md shadow-lg py-5  px-10 my-40 w-9/12 h-9/12">
      {!response?.success ? (
        <div className="mx-4 md:mx-0">
          <h3 className="text-black text-[32px] font-bold">
            Become a Subscriber
          </h3>
          <p className="py-3 text-base text-black mb-4">
            Subscribers stay up to date with events and promotions. Join today
            to receive retailer openings, offers, access to exclusive events and
            more!
          </p>
          <Formik
            initialValues={initialValue}
            validateOnMount
            validationSchema={FORM_VALIDATION}
            onSubmit={fetchPostApiData}
          >
            {({ handleBlur, handleChange, values }) => (
              <Form>
                <div className="grid grid-cols-12 gap-4">
                  {FormData?.map((item) => (
                    <div className="col-span-12 md:col-span-6" key={item?.id}>
                      <TextField
                        name={item?.name}
                        type={item?.type}
                        placeholder={item?.placeholder}
                      />
                    </div>
                  ))}
                  <div className="col-span-12 md:col-span-6 ">
                    <Select
                      selectOptionsData={selectOptionsCountry}
                      values={values?.country}
                      placeHolder="select a country"
                      handleBlur={handleBlur}
                      handleChange={handleChange}
                      name={"country"}
                    />
                  </div>
                  <div className="col-span-12 md:col-span-6 ">
                    <Select
                      selectOptionsData={selectOptionsGender}
                      values={values?.gender}
                      handleBlur={handleBlur}
                      placeHolder="select your gender"
                      handleChange={handleChange}
                      name={"gender"}
                    />
                  </div>
                  <div className="col-span-12 my-10">
                    <h6 className="text-black mb-4">Interest:</h6>
                    {width <= "415" ? (
                      <div className="col-span-12 md:col-span-6 ">
                        <MultiSelect
                          options={selectOptionsInterest}
                          values={values?.interest}
                          handleBlur={handleBlur}
                          placeHolder="select your interest"
                          handleChange={handleChange}
                          name={"interest"}
						  displayValue="value"
                        />
                      </div>
                    ) : (
                      <div className="flex items-center flex-wrap col-span-12">
                        <CheckBox
                          name="interest"
                          value="freshFood"
                          text="Fresh Food"
                        />
                        <CheckBox
                          name="interest"
                          value="dining"
                          text="dining"
                        />
                        <CheckBox
                          name="interest"
                          value="entertainment"
                          text="entertainment"
                        />
                        <CheckBox
                          name="interest"
                          value="services"
                          text="services"
                        />
                        <CheckBox
                          name="interest"
                          value="beauty&health"
                          text="beauty & Health"
                        />
                      </div>
                    )}
                    <div className="col-span-12 my-10">
                      <div className="flex items-center justify-between flex-wrap">
                        <p className=" w-72 text-sm text-black">
                          By submitting your details, you agree with out{" "}
                          <span className="underline">
                            Terms and Conditions
                          </span>
                          , and have read our Privacy Policy.
                        </p>
                        <Button
                          type="submit"
                          variant="primary"
                          text="submit"
                          className="px-14 capitalize my-4 md:my-0 "
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      ) : (
        <div>
          <h1 className="text-black text-[32px] font-bold text-center uppercase my-5">
            {response.title}
          </h1>
          <p className="text-center my-7">{response.subtitle}</p>
          <div className=" flex justify-center items-center my-5">
            <Button
              type="submit"
              variant="primary"
              text="Visit The Blog"
              className=" px-12 capitalize my-4 md:my-0 "
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default React.memo(Subscriber);
