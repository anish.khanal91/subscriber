/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/**/*.{html,js}"],
	theme: {
		extend: {
			colors: {
				"black-color": "#202020",
				"pure-white": "#FFFFFF",
			},
		},
	},
	plugins: [],
};
